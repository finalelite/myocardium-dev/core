package br.com.finalelite.myocardium.account.repository;

import br.com.finalelite.myocardium.account.repository.token.AccountToken;
import br.com.finalelite.myocardium.account.repository.token.DiscordUpdateToken;
import br.com.finalelite.myocardium.account.repository.token.LoginToken;
import br.com.finalelite.myocardium.account.repository.token.RankUpdateToken;
import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.web.rsi.RestfulImpl;
import br.com.finalelite.myocardium.web.rsi.thread.CallThread;

import java.util.concurrent.ExecutionException;

public class AccountServiceImpl extends RestfulImpl {

    private AccountManager accountManager;
    private String url;

    public AccountServiceImpl(AccountManager accountManager, String url) {
        super(new CallThread());

        this.url = url;
        this.accountManager = accountManager;
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }

    public AccountToken loginAccount(String nickname) throws ExecutionException, InterruptedException {

        LoginToken token = new LoginToken();
        token.nickname = nickname;

        AccountToken accountToken = getAccount(token.nickname);
        if (accountToken == null) {
            accountToken = postForObject(url, token, AccountToken.class).get().getData();
        }
        return accountToken;
    }

    public AccountToken updateDiscord(String nickname, DiscordUpdateToken token) throws ExecutionException, InterruptedException {
        return putForObject(url + "/" + nickname, token, AccountToken.class).get().getData();
    }

    public AccountToken updateRank(String nickname, RankUpdateToken token) throws ExecutionException, InterruptedException {
        return putForObject(url + "/" + nickname, token, AccountToken.class).get().getData();
    }

    public AccountToken getAccount(String nickname) throws ExecutionException, InterruptedException {
        return getForObject(url + "/" + nickname, AccountToken.class).get().getData();
    }


}
