package br.com.finalelite.myocardium.account.internal;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import br.com.finalelite.myocardium.internal.mqtt.message.MessageType;

public class AccountMessenger {


    public void broadcastGroupUpdate(String playerName, long groups) {
        Core.mqttPublisher.sendMessage(new Message(MessageType.UPDATEGROUP, playerName, String.valueOf(groups)).build(), true);
    }



}
