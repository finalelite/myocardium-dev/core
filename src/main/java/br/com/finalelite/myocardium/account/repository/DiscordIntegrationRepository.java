package br.com.finalelite.myocardium.account.repository;

import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.internal.redis.RedisRepository;
import redis.clients.jedis.Jedis;

public class DiscordIntegrationRepository extends RedisRepository {

    private AccountManager accountManager;

    public DiscordIntegrationRepository(AccountManager accountManager) {
        super(accountManager.getPlugin());
        this.accountManager = accountManager;
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }

    public boolean validateCode(String code) {
        try (Jedis client = getResource()) {
            if (!client.exists("discord_key#" + code)) {
                return false;
            }
        }
        return true;
    }

    public String getIdFromCode(String code) {
        try (Jedis client = getResource()) {
            return client.hget("discord_key#" + code, "id");
        }
    }
}
