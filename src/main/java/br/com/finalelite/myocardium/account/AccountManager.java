package br.com.finalelite.myocardium.account;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.account.commands.UpdateGroup;
import br.com.finalelite.myocardium.account.internal.AccountMessenger;
import br.com.finalelite.myocardium.account.repository.AccountServiceImpl;
import br.com.finalelite.myocardium.account.repository.DiscordIntegrationRepository;
import br.com.finalelite.myocardium.account.repository.token.AccountToken;
import br.com.finalelite.myocardium.common.Group;
import br.com.finalelite.myocardium.module.Module;
import br.com.finalelite.myocardium.punishment.PunishmentManager;
import br.com.finalelite.myocardium.updater.UpdateType;
import br.com.finalelite.myocardium.updater.event.UpdateEvent;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class AccountManager extends Module {

    private static final Object accountLock = new Object();
    private static HashMap<String, Object> accountLoginLock = new HashMap<>();

    private AccountServiceImpl accountService;
    private AccountMessenger accountMessenger;
    private Map<String, Account> cachedAccounts;
    private HashSet<String> duplicateLoginGlitchPreventionList;
    @Getter private DiscordIntegrationRepository discordIntegrationRepository;
    @Setter private PunishmentManager punishmentManager;

    private static AtomicInteger accountsConnecting = new AtomicInteger(0);
    private static AtomicInteger accountsProcessing = new AtomicInteger(0);

    public AccountManager() {
        super("Account Manager", Core.getInstance());
    }

    @Override
    public void onEnable() {
        accountService = new AccountServiceImpl(this, System.getProperty("core.webserver") + "/account");
        cachedAccounts = new HashMap<>();
        duplicateLoginGlitchPreventionList = new HashSet<>();
        discordIntegrationRepository = new DiscordIntegrationRepository(this);
        this.accountMessenger = new AccountMessenger();
    }

    @Override
    public void addCommands() {
        addCommand(new UpdateGroup(this));
    }

    public AccountServiceImpl getAccountService() {
        return accountService;
    }

    public Map<String, Account> getCachedAccounts() {
        return cachedAccounts;
    }

    public Collection<Account> getAccounts() {
        return cachedAccounts.values();
    }

    public PunishmentManager getPunishmentManager() {
        return punishmentManager;
    }


    public AccountMessenger getAccountMessenger() {
        return accountMessenger;
    }

    public Account addAccount(String nickname) {
        Account newAccount = new Account(nickname);
        Account oldAccount;

        synchronized (accountLock) {
            oldAccount = cachedAccounts.put(nickname, newAccount);
        }

        if (oldAccount != null) {
            oldAccount.delete();
        }
        return newAccount;

    }

    public void deleteAccount(String nickname) {
        synchronized (accountLock) {
            cachedAccounts.remove(nickname);
        }
    }

    public Account getAccount(String nickname) {
        synchronized (accountLock) {
            return cachedAccounts.get(nickname);
        }
    }

    public Account getAccount(Player player) {
        return getAccount(player.getName());
    }

    public int getPlayerCountIncludingConnecting() {
        return Bukkit.getOnlinePlayers().size() + Math.max(0, accountsConnecting.get());
    }

    @EventHandler
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        try {
            accountsConnecting.incrementAndGet();

            while (accountsProcessing.get() >= 5) {
                try {
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {

                accountsProcessing.incrementAndGet();

                this.getPunishmentManager().validateSession(event);

                try {
                    if (!loadAccount(this.addAccount(event.getName()))) {
                        event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "There was a problem logging you in.");
                    }
                } catch (ExecutionException | InterruptedException e) {
                    event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "There was a problem logging you in.");
                }

            } catch (Exception e) {
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "Error retrieving information from web, please retry in a minute.");
                e.printStackTrace();
            } finally {
                accountsProcessing.decrementAndGet();
            }

        } finally {
            accountsConnecting.decrementAndGet();
        }
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        synchronized (accountLock) {
            if (!cachedAccounts.containsKey(event.getPlayer().getName())) {
                cachedAccounts.putIfAbsent(event.getPlayer().getName(), new Account(event.getPlayer().getName()));
            }

            Account account = this.getAccount(event.getPlayer().getName());

            if (account == null) {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "There was an error logging you in. Please reconnect.");
                return;
            }

            account.setPlayer(event.getPlayer());
            this.punishmentManager.computeMute(account);
        }

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        if (event.getReason().contains("You logged in from another location"))
            duplicateLoginGlitchPreventionList.add(event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (!duplicateLoginGlitchPreventionList.contains(event.getPlayer().getName())) {
            this.deleteAccount(event.getPlayer().getName());
            duplicateLoginGlitchPreventionList.remove(event.getPlayer().getName());
        }
    }

    @EventHandler
    public void cleanGlitchedAccounts(UpdateEvent event) {
        if (event.getType() != UpdateType.SLOW) {
            return;
        }

        synchronized (accountLock) {
            cachedAccounts.forEach((key, value) -> {
                Player player = value.getPlayer();

                if (player != null && !player.isOnline()) {
                    cachedAccounts.remove(key, value);
                }

            });
        }
    }

    private boolean loadAccount(final Account account) throws ExecutionException, InterruptedException {
        long timeStart = System.currentTimeMillis();

        AtomicReference<AccountToken> token = new AtomicReference<>(null);
        accountLoginLock.put(account.getNickname(), new Object());


        runAsync(() -> {
            try {

                token.set(accountService.loginAccount(account.getNickname()));
                account.setAccountId(token.get().id);
                accountLoginLock.remove(account.getNickname());
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        while (accountLoginLock.containsKey(account.getNickname()) && System.currentTimeMillis() - timeStart < 15000) {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        token.set(accountService.loginAccount(account.getNickname()));
        account.setGroups(Group.calculate(token.get().groups));

        if (token.get().discord_id != null) {
            account.setDiscordId(token.get().discord_id);
        }

        return !accountLoginLock.containsKey(account.getNickname());

    }


}
