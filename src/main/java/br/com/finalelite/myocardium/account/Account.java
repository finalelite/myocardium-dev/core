package br.com.finalelite.myocardium.account;

import org.bukkit.entity.Player;
import br.com.finalelite.myocardium.common.Group;

import java.util.List;

public class Account {

    private int accountId = -1;
    private String nickname;
    private Player player;
    private List<Group> groups;
    private String discordId;
    private boolean muted = false;
    private long expireTime;

    public Account(Player player) {
        this.player = player;
        this.nickname = player.getName();
    }

    public String getDiscordId() {
        return discordId;
    }

    public void setDiscordId(String discordId) {
        this.discordId = discordId;
    }

    public Account(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void delete() {
        this.nickname = null;
        this.player = null;
    }

    public boolean isMuted() {
        return muted;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public void setMuted(boolean muted, long expireTime) {
        this.muted = muted;
        this.expireTime = expireTime;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public void addRank(Group group) {
        groups.add(group);
    }

    public void removeRank(Group group) {
        groups.remove(group);
    }
}
