package br.com.finalelite.myocardium.account.commands;

import br.com.finalelite.myocardium.account.Account;
import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.account.repository.token.AccountToken;
import br.com.finalelite.myocardium.account.repository.token.DiscordUpdateToken;
import br.com.finalelite.myocardium.commands.CommandBase;
import br.com.finalelite.myocardium.common.Group;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class LinkAccount extends CommandBase<AccountManager> {

    public LinkAccount(AccountManager module, Group[] requiredGroups, String... aliases) {
        super(module, requiredGroups, aliases);
    }

    @Override
    public void execute(Player player, String[] args) {
        if (args == null || args.length > 1) {
            player.sendMessage(ChatColor.RED + "Uso: /" + aliasUsed + " [código]");
            return;
        }

        String code = args[0];
        Account account = this.module.getAccount(player);

        if (!this.module.getDiscordIntegrationRepository().validateCode(code)) {
            player.sendMessage(ChatColor.RED + "O código inserido é inválido.");
            return;
        }

        String discordId = this.module.getDiscordIntegrationRepository().getIdFromCode(code);

        DiscordUpdateToken token = new DiscordUpdateToken();
        token.discord_id = discordId;

        boolean success = false;

        try {
            AccountToken accountToken = this.module.getAccountService().updateDiscord(account.getNickname(), token);
            if (accountToken.discord_id.equals(discordId)) {
                success = true;
            }
        } catch (ExecutionException | InterruptedException e) {
            player.sendMessage(ChatColor.RED + "Ocorreu um erro ao se comunicar com o servidor web.");
            e.printStackTrace();
            return;
        }

        if (success) {
            player.sendMessage(ChatColor.GREEN + "Sucesso! Sua conta do Minecraft e Discord foram vinculadas.");
        }

    }

    @Override
    public List<String> onTabComplete(Player player, String command, String[] args) {
        return null;
    }
}
