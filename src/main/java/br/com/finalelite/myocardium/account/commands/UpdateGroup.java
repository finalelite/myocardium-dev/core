package br.com.finalelite.myocardium.account.commands;

import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.account.repository.token.AccountToken;
import br.com.finalelite.myocardium.account.repository.token.RankUpdateToken;
import br.com.finalelite.myocardium.commands.CommandBase;
import br.com.finalelite.myocardium.common.Group;
import br.com.finalelite.myocardium.menu.MenuInventory;
import br.com.finalelite.myocardium.menu.MenuItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UpdateGroup extends CommandBase<AccountManager> {

    public UpdateGroup(AccountManager module) {
        super(module, Group.LEADERSHIP, "updateGroup", "groups");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(Player player, String[] args) {
        module.runAsync(() -> {
            if (args == null || args.length < 1) {
                player.sendMessage(ChatColor.RED + "Uso: /" + aliasUsed + " [jogador]");
                return;
            }

            player.sendMessage(ChatColor.GRAY + "Enviando requisição...");

            AccountToken accountToken;
            try {
                accountToken = this.module.getAccountService().getAccount(args[0]);
            } catch (ExecutionException | InterruptedException e) {
                player.sendMessage(ChatColor.RED + "Ocorreu um erro ao recuperar informações do servidor web.");
                e.printStackTrace();
                return;
            }

            if (accountToken == null) {
                player.sendMessage(ChatColor.RED + "O jogador informado nunca entrou no servidor.");
                return;
            }

            MenuInventory menuInventory = new MenuInventory("Conta: " + accountToken.nickname, (4 + 2), true);
            int currentSlot = 18;

            for (Group group : Group.values()) {

                Material material;

                List<Group> targetGroups = Group.calculate(accountToken.groups);

                if (targetGroups.contains(group)) {
                    material = Material.GREEN_BANNER;
                } else {
                    material = Material.RED_BANNER;
                }

                ItemStack itemStack = new ItemStack(material);
                ItemMeta itemMeta = itemStack.getItemMeta();

                itemMeta.setDisplayName(ChatColor.BOLD + group.name().replaceAll("_", " "));

                String[] lore = new String[]{
                        "",
                        ChatColor.GRAY + "Nome: " + ChatColor.RESET + group.getName(),
                        ChatColor.GRAY + "Valor: " + ChatColor.RESET + group.getValue(),
                        ChatColor.GRAY + "Estilização: " + group.getColor() + "[" + group.getTag() + "] " + group.getColor() + accountToken.nickname,
                        "",
                        Group.calculate(accountToken.groups).contains(group) ? ChatColor.GRAY + "Clique para desativar" : ChatColor.GRAY + "Clique para ativar"
                };

                itemMeta.setLore(Arrays.asList(lore));
                itemStack.setItemMeta(itemMeta);

                menuInventory.setItem(new MenuItem(itemStack, (caller, inventory, clickType, stack, slot) -> {

                    boolean success = false;

                    RankUpdateToken updateToken = new RankUpdateToken();

                    if (!targetGroups.contains(group)) {
                        updateToken.groups = (accountToken.groups + group.getValue());
                    } else {
                        updateToken.groups = (accountToken.groups - group.getValue());
                    }



                    caller.closeInventory();
                    player.sendMessage(ChatColor.GRAY + "Enviando requisição...");
                    AccountToken newAccount = null;

                    try {
                        module.getAccountService().updateRank(accountToken.nickname, updateToken);
                        newAccount = module.getAccountService().getAccount(accountToken.nickname);

                        if (newAccount.groups != accountToken.groups) {
                            success = true;
                        }
                    } catch (ExecutionException | InterruptedException e) {
                        player.sendMessage(ChatColor.RED + "Ocorreu um erro ao atualizar informações no servidor web.");
                        e.printStackTrace();
                    }

                    if (success) {
                        player.sendMessage(ChatColor.GREEN + "Os grupos de " + newAccount.nickname + " foram atualizados com sucesso!");
                        module.getAccountMessenger().broadcastGroupUpdate(newAccount.nickname, newAccount.groups);

                    }

                }), currentSlot);
                currentSlot++;

            }

            ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
            skull = Bukkit.getUnsafe().modifyItemStack(skull, "{SkullOwner:\"" + accountToken.nickname + "\"}");

            ItemMeta skullMeta = skull.getItemMeta();
            skullMeta.setDisplayName(ChatColor.BLUE + "Grupos de " + accountToken.nickname);

            String[] lore = new String[]{
                    "",
                    ChatColor.GRAY + "ID: " + ChatColor.RESET + accountToken.id,
                    ChatColor.GRAY + "Nickname: " + ChatColor.RESET + accountToken.nickname,
                    ChatColor.GRAY + "Grupos: " + ChatColor.RESET + accountToken.groups,
                    ChatColor.GRAY + "Criado em: " + ChatColor.RESET + accountToken.created_at,
                    ChatColor.GRAY + "Atualizado em: " + ChatColor.RESET + accountToken.updated_at
            };

            skullMeta.setLore(Arrays.asList(lore));

            skull.setItemMeta(skullMeta);

            menuInventory.setItem(skull, 4);
            menuInventory.createAndOpenInventory(player);

        });


    }

    @Override
    public List<String> onTabComplete(Player player, String command, String[] args) {
        return null;
    }
}
