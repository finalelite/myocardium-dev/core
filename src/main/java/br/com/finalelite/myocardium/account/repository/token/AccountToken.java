package br.com.finalelite.myocardium.account.repository.token;

public class AccountToken {

    public int id;
    public String nickname;
    public long groups;
    public String discord_id;
    public String created_at;
    public String updated_at;

}
