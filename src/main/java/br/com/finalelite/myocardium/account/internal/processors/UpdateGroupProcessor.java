package br.com.finalelite.myocardium.account.internal.processors;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.account.Account;
import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.common.Group;
import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import br.com.finalelite.myocardium.internal.mqtt.message.Processor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.stream.Collectors;

public class UpdateGroupProcessor implements Processor {

    @Override
    public void process(Message message) {
        AccountManager accountManager = Core.accountManager;
        Player player = Bukkit.getPlayer(message.getPlayerName().toLowerCase());
        if (player == null) {
            return;
        }

        long newGroups = Long.valueOf(message.getValue());
        Account account = accountManager.getAccount(player);

        if (Group.calculate(account.getGroups()) == newGroups) {
            return;
        }

        account.setGroups(Group.calculate(newGroups));

        String groupsString = Group.calculate(newGroups).stream().map(Group::getName).collect(Collectors.joining(", "));

        player.sendMessage(ChatColor.GOLD + "Os seus grupos foram atualizados!");
        player.sendMessage(ChatColor.GOLD + "Seus grupos: " + ChatColor.RESET + groupsString);

    }
}
