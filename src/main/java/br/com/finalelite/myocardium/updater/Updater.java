package br.com.finalelite.myocardium.updater;

import br.com.finalelite.myocardium.updater.event.UpdateEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class Updater implements Runnable {

    private JavaPlugin plugin;

    public Updater(JavaPlugin plugin) {
        this.plugin = plugin;
        this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this, 0, 1);
    }

    @Override
    public void run() {
        Arrays.stream(UpdateType.values()).filter(UpdateType::elapsed).forEach(type -> plugin.getServer().getPluginManager().callEvent(new UpdateEvent(type)));
    }

}
