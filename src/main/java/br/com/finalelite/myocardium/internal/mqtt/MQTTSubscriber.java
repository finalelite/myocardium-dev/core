package br.com.finalelite.myocardium.internal.mqtt;

import lombok.Getter;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public abstract class MQTTSubscriber implements MqttCallback {

    @Getter
    private final Object clientLock = new Object();

    @Getter
    private MqttClient client;

    @Getter
    private String clientId;

    @Getter
    private static MQTTSubscriber instance;

    MQTTSubscriber(String hostname, String port) throws MqttException {
        if (instance == null) {
            instance = this;
        }

        clientId = MqttClient.generateClientId();
        client = new MqttClient("tcp://" + hostname + ":" + port, clientId);
        client.connect();


        new Thread(() -> {

            while (true) {
                if (!client.isConnected()) {
                    try {
                        client.connect();
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        client.setCallback(this);
        client.subscribe("core");


    }

    @Override
    public void connectionLost(Throwable throwable) {
        try {
            this.client.connect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        // Not implemented
    }
}
