package br.com.finalelite.myocardium.internal.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetCallable {

    void processResultSet(ResultSet resultSet) throws SQLException;

}
