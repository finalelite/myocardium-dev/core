package br.com.finalelite.myocardium.internal.mysql;

import br.com.finalelite.myocardium.internal.mysql.column.Column;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import javax.sql.DataSource;
import java.sql.*;

public abstract class RepositoryBase implements Listener {

    private DataSource dataSource;
    protected JavaPlugin plugin;

    public RepositoryBase(JavaPlugin plugin, DataSource dataSource) {
        this.plugin = plugin;
        this.dataSource = dataSource;

        Bukkit.getServer().getScheduler().runTaskAsynchronously(plugin, this::internalInitialize);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    protected abstract void initialize();

    protected abstract void update();

    private void internalInitialize() {
        initialize();
        update();
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    protected int executeUpdate(String query, Column<?>... columns) {
        return executeInsert(query, null, columns);
    }

    protected int executeInsert(String query, ResultSetCallable callable, Column<?>... columns) {
        int affectedRows = 0;

        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < columns.length; i++) {
                columns[i].setValue(preparedStatement, i + 1);
            }
            affectedRows = preparedStatement.executeUpdate();
            if (callable != null) {
                callable.processResultSet(preparedStatement.getGeneratedKeys());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return affectedRows;
    }

    protected void executeQuery(PreparedStatement statement, ResultSetCallable callable, Column<?>... columns) {
        try {

            for (int i = 0; i < columns.length; i++) {
                columns[i].setValue(statement, i + 1);
            }

            try (ResultSet resultSet = statement.executeQuery()) {
                callable.processResultSet(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void executeQuery(String query, ResultSetCallable callable, Column<?>... columns) {
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            executeQuery(preparedStatement, callable, columns);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
