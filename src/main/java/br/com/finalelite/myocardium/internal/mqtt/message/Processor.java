package br.com.finalelite.myocardium.internal.mqtt.message;

public interface Processor {

    void process(Message message);

}
