package br.com.finalelite.myocardium.internal.mqtt;

import lombok.Getter;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.concurrent.atomic.AtomicBoolean;

public class MQTTPublisher {

    @Getter
    private static final Object clientLock = new Object();

    @Getter
    private static MqttClient client;

    @Getter
    private static String clientId = null;


    public MQTTPublisher(String hostname, String port) throws MqttException {
        if (clientId == null) clientId = MqttClient.generateClientId();
        if (client == null) client = new MqttClient("tcp://" + hostname + ":" + port, clientId);
        client.connect();

        new Thread(() -> {
            while (true) {
                synchronized (clientLock) {
                    if (!client.isConnected()) {
                        try {
                            client.connect();
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

    }

    private boolean retry(byte[] payload) {
        try {
            Thread.sleep(10000);
            sendMessage(payload, false);
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public synchronized boolean sendMessage(byte[] payload, boolean retry) {
        AtomicBoolean success = new AtomicBoolean(false);

        synchronized (clientLock) {
            if (!client.isConnected()) {
                try {
                    client.connect();
                } catch (MqttException e) {
                    if (retry) success.set(retry(payload));
                    return false;
                }
            }
            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setPayload(payload);

            try {
                client.publish("core", mqttMessage);
                success.set(true);
            } catch (MqttException e) {
                if (retry) success.set(retry(payload));
                e.printStackTrace();
            }
        }
        return success.get();

    }

    public synchronized boolean sendMessage(String message, boolean retry) {
        return sendMessage(message.getBytes(), retry);
    }

}
