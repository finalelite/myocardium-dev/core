package br.com.finalelite.myocardium.internal.mqtt;

import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class GlobalSubscriber extends MQTTSubscriber {


    public GlobalSubscriber(String hostname, String port) throws MqttException {
        super(hostname, port);
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) {
        Message.processMessage(mqttMessage.toString()).process();
    }
}
