package br.com.finalelite.myocardium.internal.mqtt.message;

import br.com.finalelite.myocardium.account.internal.processors.UpdateGroupProcessor;
import br.com.finalelite.myocardium.punishment.internal.processors.BanProcessor;
import br.com.finalelite.myocardium.punishment.internal.processors.MuteProcessor;
import br.com.finalelite.myocardium.punishment.internal.processors.RevokeProcessor;

public enum MessageType {

    UPDATEGROUP("5550", new UpdateGroupProcessor()),
    BANUSER("4241", new BanProcessor()),
    MUTEUSER("4d55", new MuteProcessor()),
    FORGIVE("464f", new RevokeProcessor());

    private String identifier;
    private Processor processor;

    MessageType(String identifier, Processor processor) {
        this.identifier = identifier;
        this.processor = processor;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Processor getProcessor() {
        return processor;
    }
}
