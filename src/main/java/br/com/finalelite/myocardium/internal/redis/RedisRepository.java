package br.com.finalelite.myocardium.internal.redis;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public abstract class RedisRepository implements Listener {

    private JedisPool pool;
    private JavaPlugin plugin;

    public RedisRepository(JavaPlugin plugin, JedisPool pool) {
        this.plugin = plugin;
        this.pool = pool;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public RedisRepository(JavaPlugin plugin) {
        this(plugin, RedisPool.DEFAULT);
    }

    protected JedisPool getPool() {
        return pool;
    }

    protected JavaPlugin getPlugin() {
        return plugin;
    }

    protected Jedis getResource() {
        return pool.getResource();
    }


}
