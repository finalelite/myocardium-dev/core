package br.com.finalelite.myocardium.internal.mysql.column;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnText extends Column<String> {

    public int Length = 25;

    public ColumnText(String name, int length) {
        this(name, length, "");
    }

    public ColumnText(String name, int length, String value) {
        super(name);

        Length = length;
        this.value = value;
    }

    public String getCreateString() {
        return name + " VARCHAR(" + Length + ")";
    }

    @Override
    public String getValue(ResultSet resultSet) throws SQLException {
        return resultSet.getString(name);
    }

    @Override
    public void setValue(PreparedStatement preparedStatement, int columnNumber) throws SQLException {
        preparedStatement.setString(columnNumber, value);
    }

    @Override
    public ColumnText clone() {
        return new ColumnText(name, Length, value);
    }
}