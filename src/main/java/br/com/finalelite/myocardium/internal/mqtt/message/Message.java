package br.com.finalelite.myocardium.internal.mqtt.message;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Base64;

@Data
@AllArgsConstructor
public class Message {

    private MessageType type;
    private String playerName;
    private String value;

    public String build() {
        return type.getIdentifier() + playerName.toUpperCase() + ":" + Base64.getEncoder().encodeToString(value.getBytes());
    }

    public static Message processMessage(String raw) {
        MessageType type = null;
        for (MessageType messageType : MessageType.values()) {
            if (messageType.getIdentifier().equalsIgnoreCase(raw.substring(0, Math.min(raw.length(), 4)))) {
                if (type == null) {
                    type = messageType;
                }
            }
        }

        String playerName = raw.substring(4).split(":")[0];
        String value = new String(Base64.getDecoder().decode(raw.substring(4).split(":")[1]));
        return new Message(type, playerName, value);
    }

    public void process() {
        this.type.getProcessor().process(this);
    }

}