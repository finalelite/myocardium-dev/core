package br.com.finalelite.myocardium;

import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.commands.CommandCenter;
import br.com.finalelite.myocardium.internal.mqtt.GlobalSubscriber;
import br.com.finalelite.myocardium.internal.mqtt.MQTTPublisher;
import br.com.finalelite.myocardium.internal.redis.RedisPool;
import br.com.finalelite.myocardium.menu.MenuManager;
import br.com.finalelite.myocardium.punishment.PunishmentManager;
import br.com.finalelite.myocardium.updater.Updater;
import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.eclipse.paho.client.mqttv3.MqttException;

public class Core extends JavaPlugin {

    private static Gson gson;
    private static Core instance;
    public static AccountManager accountManager;
    public static PunishmentManager punishmentManager;

    public static GlobalSubscriber globalSubscriber;
    public static MQTTPublisher mqttPublisher;

    @Override
    public void onEnable() {

        saveDefaultConfig();
        System.setProperty("core.webserver", getConfig().getString("webserver"));

        try {
            mqttPublisher = new MQTTPublisher("127.0.0.1", "1883");
            globalSubscriber = new GlobalSubscriber("127.0.0.1", "1883");
        } catch (MqttException e) {
            System.out.println("Não foi possível conectar ao MQTT.");
            e.printStackTrace();
            Bukkit.shutdown();
        }

        CommandCenter.initialize(this);
        accountManager = new AccountManager();
        CommandCenter.instance.setAccountManager(accountManager);

        punishmentManager = new PunishmentManager(accountManager);
        accountManager.setPunishmentManager(punishmentManager);

        new MenuManager(this);

        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Updater(this), 1, 1);

    }

    @Override
    public void onLoad() {
        gson = new Gson();
        instance = this;

    }

    @Override
    public void onDisable() {
        super.onDisable();
        RedisPool.DEFAULT.close();
    }


    public static Gson getGson() {
        return gson;
    }

    public static Core getInstance() {
        return instance;
    }
}
