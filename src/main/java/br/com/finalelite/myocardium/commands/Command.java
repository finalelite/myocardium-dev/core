package br.com.finalelite.myocardium.commands;

import org.bukkit.entity.Player;
import br.com.finalelite.myocardium.common.Group;

import java.util.Collection;
import java.util.List;

public interface Command {

    void setCommandCenter(CommandCenter commandCenter);

    void execute(Player player, String[] args);

    Collection<String> getAliases();

    void setAliasUsed(String aliasUsed);

    Group[] getRequiredGroups();

    List<String> onTabComplete(Player player, String command, String[] args);

}
