package br.com.finalelite.myocardium.commands;

import br.com.finalelite.myocardium.account.Account;
import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.commands.protocol.features.CustomMessage;
import br.com.finalelite.myocardium.common.Group;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.help.HelpTopic;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CommandCenter implements Listener {

    public static CommandCenter instance;

    protected JavaPlugin plugin;
    protected AccountManager accountManager;
    protected HashMap<String, Command> commands;

    public static void initialize(JavaPlugin plugin) {
        if (instance == null) {
            instance = new CommandCenter(plugin);
        }
    }

    private static final String[] blockedCommands = new String[] {
            "me",
            "tell"
    };

    private CommandCenter(JavaPlugin plugin) {
        this.plugin = plugin;
        this.commands = new HashMap<>();

        this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
        CustomMessage.enable();
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String commandName = event.getMessage().substring(1);
        String[] args = null;

        if (commandName.contains(":")) {
            event.getPlayer().sendMessage(ChatColor.RED + "Você não pode enviar comandos que contenham ':' (dois pontos).");
            event.setCancelled(true);
            return;
        }

        if (commandName.contains(" ")) {
            commandName = commandName.split(" ")[0];
            args = event.getMessage().substring(event.getMessage().indexOf(' ') + 1).split(" ");
        }

        if (Arrays.asList(blockedCommands).contains(commandName)) {
            event.getPlayer().sendMessage(ChatColor.RED + "Esse comando foi bloqueado.");
            event.setCancelled(true);
            return;
        }

        Command command = this.commands.get(commandName.toLowerCase());

        if (command != null) {

            event.setCancelled(true);

            Account account = accountManager.getAccount(event.getPlayer());

            if (Group.hasRank(account.getPlayer(), account.getGroups(), command.getRequiredGroups(), true)) {
                command.setAliasUsed(commandName.toLowerCase());
                command.execute(event.getPlayer(), args);
            }

        } else {
            HelpTopic helpTopic = Bukkit.getHelpMap().getHelpTopic(event.getMessage().split(" ")[0]);
            if (helpTopic == null) {
                event.getPlayer().sendMessage(ChatColor.RED + "Comando não encontrado.");
                event.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void onTabComplete(TabCompleteEvent event) {
        CommandSender commandSender = event.getSender();
        if (!(commandSender instanceof Player)) {
            return;
        }
        Player player = (Player) commandSender;

        String commandName = event.getBuffer().substring(1);
        String[] args = null;

        if (commandName.contains(" ")) {
            commandName = commandName.split(" ")[0];
            args = event.getBuffer().substring(event.getBuffer().indexOf(' ') + 1).split(" ");
        }

        Command command = commands.get(commandName.toLowerCase());

        if (command != null) {

            Account playerAccount = accountManager.getAccount(player);
            if (!Group.hasRank(playerAccount.getGroups(), command.getRequiredGroups())) {
                event.setCompletions(new ArrayList<>());
                return;
            }

            List<String> suggestions = command.onTabComplete(player, commandName, args);
            if (suggestions != null) {
                event.setCompletions(suggestions);
            }

        }

    }

    public void addCommand(Command command) {
        command.getAliases().forEach(alias -> {
            commands.put(alias.toLowerCase(), command);
            command.setCommandCenter(this);
        });
    }

    public void removeCommand(Command command) {
        command.getAliases().forEach(alias -> {
            commands.remove(alias.toLowerCase());
            command.setCommandCenter(null);
        });
    }


}
