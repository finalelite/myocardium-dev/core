package br.com.finalelite.myocardium.commands.protocol;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Primitives {

    public static Object transformPrimitives(Object value, Object parent, Processor processor) {
        if (value instanceof JSONObject) {
            return transformPrimitives((JSONObject) value, processor);
        } else if (value instanceof JSONArray) {
            return transformPrimitives((JSONArray) value, processor);
        } else {
            return processor.process(value, parent);
        }
    }

    @SuppressWarnings("unchecked")
    private static JSONObject transformPrimitives(JSONObject source, Processor processor) {
        for (Object key : source.keySet().toArray()) {
            Object value = source.get(key);
            source.put(key, transformPrimitives(value, source, processor));
        }
        return source;
    }

    @SuppressWarnings("unchecked")
    private static JSONArray transformPrimitives(JSONArray source, Processor processor) {
        for (int i = 0; i < source.size(); i++) {
            Object value = source.get(i);
            source.set(i, transformPrimitives(value, source, processor));
        }
        return source;
    }

}
