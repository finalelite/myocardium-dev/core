package br.com.finalelite.myocardium.commands.protocol.features;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.commands.protocol.Primitives;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import org.bukkit.ChatColor;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CustomMessage {

    public static void enable() {
        ProtocolLibrary.getProtocolManager().addPacketListener(
                new PacketAdapter(Core.getInstance(), PacketType.Play.Server.CHAT) {
                    private JSONParser parser = new JSONParser();

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        PacketContainer packet = event.getPacket();
                        StructureModifier<WrappedChatComponent> components = packet.getChatComponents();

                        try {

                            Object data = parser.parse(components.read(0).getJson());
                            final boolean[] result = new boolean[1];

                            Primitives.transformPrimitives(data, null, (value, parent) -> {

                                if (value instanceof String) {
                                    String stripped = ChatColor.stripColor((String) value);

                                    if (stripped.equals("I'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error.")) {
                                        result[0] = true;

                                        if (parent instanceof JSONObject) {
                                            ((JSONObject) parent).put("color", "red");
                                        }
                                        return "Permissões insuficientes.";
                                    }
                                }

                                return value;
                            });

                            if (result[0]) {
                                components.write(0, WrappedChatComponent.fromJson(JSONValue.toJSONString(data)));
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

}
