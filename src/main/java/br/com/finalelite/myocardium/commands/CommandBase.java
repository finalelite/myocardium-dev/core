package br.com.finalelite.myocardium.commands;

import org.bukkit.entity.Player;
import br.com.finalelite.myocardium.common.Group;
import br.com.finalelite.myocardium.common.util.ServerUtilities;
import br.com.finalelite.myocardium.module.Module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class CommandBase<T extends Module> implements Command {

    private Group[] requiredGroups;

    private List<String> aliases;

    protected T module;
    protected String aliasUsed;
    protected CommandCenter commandCenter;

    public CommandBase(T module, Group[] requiredGroups, String... aliases) {
        this.module = module;
        this.requiredGroups = requiredGroups == null ? Group.ALL_PLAYERS : requiredGroups;
        this.aliases = Arrays.asList(aliases);
    }

    @Override
    public List<String> getAliases() {
        return aliases;
    }

    @Override
    public void setAliasUsed(String aliasUsed) {
        this.aliasUsed = aliasUsed;
    }

    @Override
    public Group[] getRequiredGroups() {
        return requiredGroups;
    }

    @Override
    public void setCommandCenter(CommandCenter commandCenter) {
        this.commandCenter = commandCenter;
    }

    protected List<String> getMatches(String start, List<String> possibleMatches) {
        return possibleMatches.stream()
                .filter(match -> match.toLowerCase().startsWith(start.toLowerCase()))
                .collect(Collectors.toList());

    }

    protected List<String> getPlayerMatches(Player sender, String start) {
        List<String> matches = new ArrayList<>();
        ServerUtilities.getPlayers().stream().filter(player -> sender.canSee(player) &&
                player.getName().toLowerCase().startsWith(start.toLowerCase()))
                .forEach(player -> matches.add(player.getName()));
        return matches;

    }
}
