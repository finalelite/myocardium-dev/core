package br.com.finalelite.myocardium.commands.protocol;

public interface Processor {

    Object process(Object value, Object parent);

}
