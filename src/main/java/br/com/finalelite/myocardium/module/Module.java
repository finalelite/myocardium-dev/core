package br.com.finalelite.myocardium.module;

import br.com.finalelite.myocardium.commands.Command;
import br.com.finalelite.myocardium.commands.CommandCenter;
import org.bukkit.Server;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.Collection;
import java.util.HashMap;

public abstract class Module implements IModule, Listener {

    private String name;
    private JavaPlugin plugin;

    private Server server;
    private BukkitScheduler scheduler;
    private PluginManager pluginManager;

    private HashMap<String, Command> commands;


    public Module(String name, JavaPlugin plugin) {
        this.name = name;
        this.plugin = plugin;

        this.server = plugin.getServer();
        this.scheduler = server.getScheduler();
        this.pluginManager = server.getPluginManager();

        this.commands = new HashMap<>();

        enable();
        addCommands();
    }

    public JavaPlugin getPlugin() {
        return plugin;
    }

    public String getName() {
        return name;
    }

    public BukkitScheduler getScheduler() {
        return scheduler;
    }

    public Collection<Command> getCommands() {
        return commands.values();
    }

    public final void addCommand(Command command) {
        CommandCenter.instance.addCommand(command);
    }

    public final void removeCommand(Command command) {
        CommandCenter.instance.removeCommand(command);
    }

    public Server getServer() {
        return server;
    }

    public PluginManager getPluginManager() {
        return pluginManager;
    }

    private void enable() {
        long epoch = System.currentTimeMillis();
        System.out.println(String.format("[%s] Inicializando...", name));

        onEnable();
        registerSelf();
        System.out.println(String.format("[%s] Inicializado em %s ms", name, (System.currentTimeMillis() - epoch)));
    }

    public void registerEvents(Listener listener) {
        pluginManager.registerEvents(listener, plugin);
    }

    private void registerSelf() {
        registerEvents(this);
    }

    public void runAsync(Runnable runnable) {
        scheduler.runTaskAsynchronously(plugin, runnable);
    }

    public void runSync(Runnable runnable) {
        scheduler.runTask(plugin, runnable);
    }

    public void runSyncLater(Runnable runnable, long delay) {
        scheduler.runTaskLater(plugin, runnable, delay);
    }
}
