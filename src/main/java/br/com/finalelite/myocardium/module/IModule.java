package br.com.finalelite.myocardium.module;

public interface IModule {

    void onEnable();

    void addCommands();

}
