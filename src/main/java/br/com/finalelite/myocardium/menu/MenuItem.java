package br.com.finalelite.myocardium.menu;

import org.bukkit.inventory.ItemStack;

public class MenuItem {

    private ItemStack itemStack;
    private MenuClickHandler handler;

    public MenuItem(ItemStack itemStack) {
        this.itemStack = itemStack;
        this.handler = (player, inventory, clickType, itemStack1, slot) -> {};
    }

    public MenuItem(ItemStack itemStack, MenuClickHandler handler) {
        this.itemStack = itemStack;
        this.handler = handler;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public MenuClickHandler getHandler() {
        return handler;
    }

    public void destroy() {
        this.itemStack = null;
        this.handler = null;
    }
}
