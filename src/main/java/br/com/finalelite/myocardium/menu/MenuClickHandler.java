package br.com.finalelite.myocardium.menu;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface MenuClickHandler {

    void onClick(Player player, Inventory inventory, ClickType clickType, ItemStack itemStack, int slot);

}
