package br.com.finalelite.myocardium.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

@Data
@AllArgsConstructor
public class MenuHolder implements InventoryHolder {

    private MenuInventory menuInventory;

    public void destroy() {
        menuInventory = null;
    }

    public boolean isOnePerPlayer() {
        return menuInventory.isOnePerPlayer();
    }

    @Override
    public Inventory getInventory() {
        if (menuInventory.isOnePerPlayer()) return null;
        return menuInventory.getInventory();
    }
}
