package br.com.finalelite.myocardium.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class MenuInventory {

    private HashMap<Integer, MenuItem> slotItem;
    private int rows;
    private String title;
    private Inventory inventory;
    private boolean onePerPlayer;

    public MenuInventory(String title, int rows, boolean onePerPlayer) {
        this.slotItem = new HashMap<>();
        this.rows = rows;
        this.title = title;
        this.onePerPlayer = onePerPlayer;
        if (!onePerPlayer) {
            this.inventory = Bukkit.createInventory(new MenuHolder(this), rows * 9, title);
        }
    }

    public void addItem(MenuItem item) {
        setItem(item, firstEmpty());
    }

    public void addItem(ItemStack itemStack) {
        setItem(itemStack, firstEmpty());
    }


    public void setItem(ItemStack item, int slot) {
        setItem(new MenuItem(item), slot);
    }

    public void setItem(MenuItem item, int slot) {
        this.slotItem.put(slot, item);
        if (!onePerPlayer) {
            inventory.setItem(slot, item.getItemStack());
        }
    }

    public int firstEmpty() {
        if (!onePerPlayer) {
            return inventory.firstEmpty();
        }

        for (int i = 0; i < rows * 9; i++) {
            if (!slotItem.containsKey(i)) {
                return i;
            }
        }

        return -1;
    }

    public boolean hasItem(int slot) {
        return this.slotItem.containsKey(slot);
    }

    public MenuItem getItem(int slot) {
        return this.slotItem.get(slot);
    }

    public void open(Player player) {
        if (!onePerPlayer) player.openInventory(inventory);
        InventoryView openInventory = player.getOpenInventory();
        if (openInventory == null || openInventory.getTopInventory().getType() != InventoryType.CHEST
                || openInventory.getTopInventory().getSize() != rows * 9 || openInventory.getTopInventory().getHolder() != null
                || !(openInventory.getTopInventory().getHolder() instanceof MenuHolder)
                || !(((MenuHolder) openInventory.getTopInventory().getHolder()).isOnePerPlayer())) {
            createAndOpenInventory(player);
            return;
        }
        for (int i = 0; i < rows * 9; i++) {
            if (slotItem.containsKey(i)) {
                player.getOpenInventory().getTopInventory().setItem(i, slotItem.get(i).getItemStack());
            } else {
                player.getOpenInventory().getTopInventory().setItem(i, null);
            }
        }
        player.updateInventory();
        ((MenuHolder) player.getOpenInventory().getTopInventory().getHolder()).setMenuInventory(this);

    }

    public void close(Player player) {
        if (onePerPlayer) {
            destroy(player);
        }
    }

    public void createAndOpenInventory(Player player) {
        Inventory inventory = Bukkit.createInventory(new MenuHolder(this), rows * 9, title);
        slotItem.entrySet().forEach(entry -> inventory.setItem(entry.getKey(), entry.getValue().getItemStack()));
        player.openInventory(inventory);
    }

    public void destroy(Player player) {
        if (player.getOpenInventory().getTopInventory().getHolder() != null &&
                player.getOpenInventory().getTopInventory().getHolder() instanceof MenuHolder) {
            ((MenuHolder) player.getOpenInventory().getTopInventory().getHolder()).destroy();
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOnePerPlayer() {
        return onePerPlayer;
    }

    public Inventory getInventory() {
        return inventory;
    }

}
