package br.com.finalelite.myocardium.menu;

import br.com.finalelite.myocardium.module.Module;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.plugin.java.JavaPlugin;

public class MenuManager extends Module {


    public MenuManager(JavaPlugin plugin) {
        super("Menu", plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory() != null) {

            if (event.getInventory().getType() == InventoryType.CHEST) {

                if (event.getInventory().getHolder() != null) {

                    if (event.getInventory().getHolder() instanceof MenuHolder) {

                        if (event.getClickedInventory() == event.getInventory()) {

                            if (event.getWhoClicked() instanceof Player) {

                                if (event.getSlot() >= 0) {

                                    MenuHolder holder = (MenuHolder) event.getInventory().getHolder();
                                    MenuInventory menu = holder.getMenuInventory();

                                    if (menu.hasItem(event.getSlot())) {
                                        Player player = (Player) event.getWhoClicked();
                                        MenuItem item = menu.getItem(event.getSlot());
                                        item.getHandler().onClick(player, event.getInventory(), ((event.getAction() == InventoryAction.PICKUP_HALF) ? ClickType.RIGHT : ClickType.LEFT), event.getCurrentItem(), event.getSlot());
                                    }
                                }
                            }
                        }
                        event.setCancelled(true);
                    }
                }
            }
        }
    }


    @Override
    public void onEnable() {

    }

    @Override
    public void addCommands() {

    }
}
