package br.com.finalelite.myocardium.web.rsi.thread;

import br.com.finalelite.myocardium.web.rsi.authorization.Authorization;
import br.com.finalelite.myocardium.web.rsi.builder.BuildRequest;
import br.com.finalelite.myocardium.web.rsi.response.Response;
import lombok.AllArgsConstructor;
import br.com.finalelite.myocardium.web.rsi.RestfulImpl;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.BiFunction;

@AllArgsConstructor
public class CallThread implements ICallThread {

    private Authorization authorization;
    private Executor executor = Executors.newCachedThreadPool();

    public CallThread() {
        this.authorization = null;
    }

    public CallThread(Authorization authorization) {
        this.authorization = authorization;
    }

    public CallThread(Executor executor) {
        this.executor = executor;
    }


    @SuppressWarnings("unchecked")
    @Override
    public <T> CompletableFuture<Response<T>> execute(String method, String endpoint, Object data,
                                                      Class<T> responseType, String... variables) {
        return CompletableFuture.supplyAsync(() -> {
            HttpURLConnection connection = null;

            try {
                connection = BuildRequest.buildHttpConnection(authorization, method, endpoint, variables);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (data != null) {
                assert connection != null;
                connection.setDoOutput(true);
                DataOutputStream dataOutputStream = null;
                try {
                    dataOutputStream = new DataOutputStream(connection.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                byte[] postData = RestfulImpl.getGson().toJson(data).getBytes();
                try {
                    assert dataOutputStream != null;
                    dataOutputStream.write(postData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return connection;
        }, executor).handle((BiFunction<HttpURLConnection, Throwable, Response<T>>) (connection, throwable) -> {
            if (throwable != null) {
                throwable.printStackTrace();
            } else {

                try {
                    if ((connection != null) && (connection.getResponseCode() == 202 ||
                            connection.getResponseCode() == 200)) {
                        String response;

                        response = BuildRequest.readInput(connection);
                        if (responseType != null) {
                            return new Response<>(RestfulImpl.getGson().fromJson(response, responseType));
                        }

                        return new Response(response);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return new Response(null);

        });
    }
}
