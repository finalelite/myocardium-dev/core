package br.com.finalelite.myocardium.web.rsi;

import br.com.finalelite.myocardium.web.rsi.response.Response;
import br.com.finalelite.myocardium.web.rsi.thread.CallThread;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.CompletableFuture;


@AllArgsConstructor
public abstract class RestfulImpl {

    @Getter
    private static final Gson gson;

    static {
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").disableHtmlEscaping().create();
    }

    private final CallThread callThread;

    public CompletableFuture<Response<String>> get(String url) {
        return callThread.execute("GET", url, null, null);
    }

    public <T> CompletableFuture<Response<T>> getForObject(String url, Class<T> responseType) {
        return callThread.execute("GET", url, null, responseType);
    }

    public <T> CompletableFuture<Response<T>> getForObject(String url, Class<T> responseType, String... variables) {
        return callThread.execute("GET", url, null, responseType, variables);
    }

    public <T> CompletableFuture<Response<T>> post(String url, Object post) {
        return callThread.execute("POST", url, post, null);
    }

    public <T> CompletableFuture<Response<T>> postForObject(String url, Object post, Class<T> responseType) {
        return callThread.execute("POST", url, post, responseType);
    }

    public <T> CompletableFuture<Response<T>> postForObject(String url, Object post, Class<T> responseType, String... variables) {
        return callThread.execute("POST", url, post, responseType, variables);
    }

    public <T> CompletableFuture<Response<T>> put(String url, Object post) {
        return callThread.execute("PUT", url, post, null);
    }

    public <T> CompletableFuture<Response<T>> putForObject(String url, Object post, Class<T> responseType) {
        return callThread.execute("PUT", url, post, responseType);
    }

    public <T> CompletableFuture<Response<T>> putForObject(String url, Object post, Class<T> responseType, String... variables) {
        return callThread.execute("PUT", url, post, responseType, variables);
    }

    public <T> CompletableFuture<Response<T>> patch(String url, Object post) {
        return callThread.execute("PATCH", url, post, null);
    }

    public <T> CompletableFuture<Response<T>> patchForObject(String url, Object post, Class<T> responseType) {
        return callThread.execute("PATCH", url, post, responseType);
    }

    public <T> CompletableFuture<Response<T>> patchForObject(String url, Object post, Class<T> responseType, String... variables) {
        return callThread.execute("PATCH", url, post, responseType, variables);
    }

    public <T> CompletableFuture<Response<T>> delete(String url) {
        return callThread.execute("DELETE", url, null, null);
    }

    public <T> CompletableFuture<Response<T>> deleteForObject(String url, Class<T> responseType) {
        return callThread.execute("DELETE", url, null, responseType);
    }

    public <T> CompletableFuture<Response<T>> deleteForObject(String url, Class<T> responseType, String... variables) {
        return callThread.execute("DELETE", url, null, responseType, variables);
    }

}
