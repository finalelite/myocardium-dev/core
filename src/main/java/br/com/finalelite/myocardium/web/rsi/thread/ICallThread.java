package br.com.finalelite.myocardium.web.rsi.thread;

import br.com.finalelite.myocardium.web.rsi.response.Response;

import java.util.concurrent.CompletableFuture;

@FunctionalInterface
public interface ICallThread {

    <T> CompletableFuture<Response<T>> execute(String method, String endpoint, Object data, Class<T> responseType, String... variables);

}
