package br.com.finalelite.myocardium.web.rsi.builder;

import br.com.finalelite.myocardium.web.rsi.authorization.Authorization;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BuildRequest {

    public static HttpURLConnection buildHttpConnection(Authorization authorization, String method, String endpoint,
                                                        String... variables) throws IOException {
        URL url = new URL(variables != null ? buildUrl(endpoint, variables) : endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        if (method.equalsIgnoreCase("PATCH")) {
            method = "POST";
            connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        }

        connection.setRequestMethod(method);
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");

        if (authorization != null) {
            connection.setRequestProperty(authorization.getName(), authorization.getToken());
        }

        return connection;
    }

    public static String readInput(HttpURLConnection httpURLConnection) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
        String inputLine;
        StringBuilder stringBuilder = new StringBuilder();

        while ((inputLine = bufferedReader.readLine()) != null) stringBuilder.append(inputLine);
        bufferedReader.close();

        return stringBuilder.toString();

    }

    private static String buildUrl(String endpoint, String... variables) {
        List<String> variable = Arrays.stream(endpoint.split("/"))
                .filter(v -> v.startsWith("{") && v.endsWith("}"))
                .collect(Collectors.toCollection(LinkedList::new));

        AtomicReference<String> transformed = new AtomicReference<>(endpoint);
        IntStream.range(0, variable.size()).forEach(i -> transformed.set(transformed.get().replace(
                variable.get(i),
                variables[i]
        )));

        return transformed.get();
    }

}
