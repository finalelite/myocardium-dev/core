package br.com.finalelite.myocardium.web.rsi.authorization.impl;

import br.com.finalelite.myocardium.web.rsi.authorization.Authorization;

import java.util.Base64;

public class BasicAuthorization implements Authorization {

    private String username;
    private String password;

    public BasicAuthorization(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String getName() {
        return "Authorization";
    }

    @Override
    public String getToken() {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
    }
}
