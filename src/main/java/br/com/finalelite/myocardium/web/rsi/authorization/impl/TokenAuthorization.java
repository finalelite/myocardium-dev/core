package br.com.finalelite.myocardium.web.rsi.authorization.impl;

import br.com.finalelite.myocardium.web.rsi.authorization.Authorization;

public class TokenAuthorization implements Authorization {

    private String bearer;

    public TokenAuthorization(String bearer) {
        this.bearer = bearer;
    }

    @Override
    public String getName() {
        return "Authorization";
    }

    @Override
    public String getToken() {
        return "Bearer " + bearer;
    }
}
