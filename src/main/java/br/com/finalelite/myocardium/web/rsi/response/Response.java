package br.com.finalelite.myocardium.web.rsi.response;

import lombok.Data;
import br.com.finalelite.myocardium.web.rsi.RestfulImpl;

@Data
public class Response<T> {

    private final T data;

    public <T> Response<T> map(Class<T> clazz) {
        if (data instanceof String) {
            return new Response<T>(RestfulImpl.getGson().fromJson((String) data, clazz));
        }
        return null;
    }

}
