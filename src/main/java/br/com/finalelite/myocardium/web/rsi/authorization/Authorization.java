package br.com.finalelite.myocardium.web.rsi.authorization;

public interface Authorization {

    String getName();
    String getToken();

}
