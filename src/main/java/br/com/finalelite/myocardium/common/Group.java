package br.com.finalelite.myocardium.common;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public enum Group {


    ADMINISTRADOR("Admin", ChatColor.RED, 8),
    GAMEMASTER("GameMaster", ChatColor.GOLD, 2),
    MEMBRO("Membro", ChatColor.GRAY, 1),
    MINIGAME_CONDE("Conde", ChatColor.YELLOW, 16384),
    MINIGAME_DUQUE("Duque", ChatColor.DARK_RED, 4096),
    MINIGAME_LORD("Lord", ChatColor.AQUA, 8192),
    MINIGAME_TITAN("Titan", ChatColor.DARK_PURPLE, 2048),
    MIXED_UP_CONDE("Conde", ChatColor.YELLOW, 512),
    MIXED_UP_DUQUE("Duque", ChatColor.DARK_RED, 128),
    MIXED_UP_LORD("Lord", ChatColor.AQUA, 256),
    MIXED_UP_TITAN("Titan", ChatColor.DARK_PURPLE, 64),
    MODERADOR("Moderador", ChatColor.DARK_GREEN, 16),
    SUPERVISOR("Supervisor", ChatColor.DARK_RED, 4),
    SUPORTE("Suporte", ChatColor.GREEN, 32),
    YOUTUBER("YouTuber", ChatColor.RED, 1024);

    public static final Group[] STAFF;
    public static final Group[] MINIGAME_VIPS;
    public static final Group[] MIXED_UP_VIPS;
    public static final Group[] LEADERSHIP;

    static {

        STAFF = new Group[]{
                ADMINISTRADOR,
                GAMEMASTER,
                MODERADOR,
                SUPERVISOR,
                SUPORTE
        };

        MINIGAME_VIPS = new Group[]{
                MINIGAME_CONDE,
                MINIGAME_DUQUE,
                MINIGAME_LORD,
                MINIGAME_TITAN,
                YOUTUBER
        };

        MIXED_UP_VIPS = new Group[]{
                MIXED_UP_CONDE,
                MIXED_UP_DUQUE,
                MIXED_UP_LORD,
                MIXED_UP_TITAN,
                YOUTUBER
        };

        LEADERSHIP = new Group[]{
                GAMEMASTER,
                SUPERVISOR
        };

        ALL_PLAYERS = Group.values();

    }

    public static final Group[] ALL_PLAYERS;


    @Getter
    private String name;

    @Getter
    private ChatColor color;

    @Getter
    private long value;

    Group(String name, ChatColor color, long value) {
        this.name = name;
        this.color = color;
        this.value = value;
    }

    public static List<Group> calculate(long value) {
        return Arrays.stream(Group.values()).filter(rank -> ((value & rank.value) == rank.value)).collect(Collectors.toList());
    }

    public static long calculate(List<Group> groupList) {
        AtomicLong sum = new AtomicLong(0);
        groupList.forEach(rank -> {
            sum.addAndGet(rank.value);
        });
        return sum.get();
    }

    public static boolean hasRank(List<Group> groups, Group group) {
        return hasRank(groups, new Group[]{group});
    }

    public static boolean hasRank(List<Group> groups, Group[] arrayOfGroups) {
        return hasRank(null, groups, arrayOfGroups, false);
    }

    public static boolean hasRank(Player player, List<Group> groups, Group[] arrayOfGroups, boolean inform) {
        if (inform && player == null) inform = false;
        for (Group group : arrayOfGroups) {
            if (groups.contains(group)) return true;
        }

        if (inform) {
            player.sendMessage(ChatColor.RED + "Permissões insuficientes.");
        }
        return false;
    }

    public String getTag() {
        if (name().equalsIgnoreCase("MEMBRO")) return ChatColor.GRAY + "";
        return color + name;
    }

    public boolean isPremium() {
        if (Arrays.asList(MIXED_UP_VIPS).contains(this)) return true;
        if (Arrays.asList(MINIGAME_VIPS).contains(this)) return true;
        return false;
    }

    public boolean isStaff() {
        return Arrays.asList(STAFF).contains(this);
    }
}
