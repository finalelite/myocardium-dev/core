package br.com.finalelite.myocardium.common.util.builder;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;

public class KickMessageBuilder {

    private ArrayList<String> lines;

    public KickMessageBuilder() {
        this.lines = new ArrayList<>();
    }

    public KickMessageBuilder addLine(String line) {
        if (line.contains("\n")) {
            String[] internalLines = line.split("\n");
            Arrays.stream(internalLines).forEach(internalLine -> this.lines.add(line));
            return this;
        }

        this.lines.add(line);
        return this;
    }

    public KickMessageBuilder addLine() {
        this.lines.add("");
        return this;
    }

    public String build() {
        StringBuilder builder = new StringBuilder();
        builder.append("&6&lFINAL ELITE").append("\n\n");
        lines.forEach(line -> builder.append(line).append("\n"));
        builder.append("&7&owww.finalelite.com.br");
        return ChatColor.translateAlternateColorCodes('&', builder.toString());
    }

}
