package br.com.finalelite.myocardium.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtilities {

    public static final String DATE_FORMAT_NOW = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_DAY = "dd/MM/yyyy";

    public static String now() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_NOW);
        return dateFormat.format(date);
    }

    public static String when(long time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_NOW);
        return dateFormat.format(time);
    }

    public static String date() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DAY);
        return dateFormat.format(date);
    }

    public enum TimeUnit {
        FIT,
        DAYS,
        HOURS,
        MINUTES,
        SECONDS,
        MILLISECONDS
    }

    public static String since(long epoch) {
        return convertString(System.currentTimeMillis() - epoch, 0, TimeUnit.FIT);
    }

    private static TimeUnit fit(long time) {
        if (time < 6E4) return TimeUnit.SECONDS;
        if (time < 36E5) return TimeUnit.MINUTES;
        if (time < 864E5) return TimeUnit.HOURS;
        return TimeUnit.DAYS;

    }

    public static double convert(long time, int trim, TimeUnit type) {
        if (type == TimeUnit.FIT) type = fit(time);
        switch (type) {
            case DAYS:
                return MathUtilities.trim(trim, time / 8.64E7);
            case HOURS:
                return MathUtilities.trim(trim, time / 3.6E6);
            case MINUTES:
                return MathUtilities.trim(trim, time / 6E4);
            case SECONDS:
                return MathUtilities.trim(trim, time / 1E3);
            default:
                return MathUtilities.trim(trim, time);
        }

    }

    public static String makeString(long time) {
        return convertString(time, 0, TimeUnit.FIT);
    }

    public static String makeString(long time, int trim) {
        return convertString(Math.max(0, time), trim, TimeUnit.FIT);
    }

    public static String convertString(long time, int trim, TimeUnit type) {
        if (time < 0) return "Permanente";
        if (type == TimeUnit.FIT) type = fit(time);

        String text;
        double num;

        if (trim == 0) {
            if (type == TimeUnit.DAYS) text = (int) (num = (int) Math.round(time / 8.64E7)) + " dia";
            else if (type == TimeUnit.HOURS) text = (int) (num = (int) Math.round(time / 3.6E6)) + " hora";
            else if (type == TimeUnit.MINUTES) text = (int) (num = (int) Math.round(time / 6E4)) + " minuto";
            else if (type == TimeUnit.SECONDS) text = (int) (num = (int) Math.round(time / 1E3)) + " segundo";
            else text = (int) (num = time) + " milissegundo";
        } else {

            if (type == TimeUnit.DAYS) text = (num = MathUtilities.trim(trim, time / 8.64E7)) + " dia";
            else if (type == TimeUnit.HOURS) text = (num = MathUtilities.trim(trim, time / 3.6E6)) + " hora";
            else if (type == TimeUnit.MINUTES) text = (num = MathUtilities.trim(trim, time / 6E4)) + " minuto";
            else if (type == TimeUnit.SECONDS) text = (num = MathUtilities.trim(trim, time / 1E3)) + " segundo";
            else text = (num = MathUtilities.trim(trim, time)) + " milissegundo";
        }

        if (num != 1) {
            text = text + "s";
        }
        return text;

    }

    public static boolean elapsed(long from, long required) {
        return System.currentTimeMillis() - from > required;
    }

}
