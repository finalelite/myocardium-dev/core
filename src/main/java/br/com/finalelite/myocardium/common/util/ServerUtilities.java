package br.com.finalelite.myocardium.common.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class ServerUtilities {

    public static List<Player> getPlayers() {
        return Arrays.asList(Bukkit.getOnlinePlayers().toArray(new Player[0]));
    }

}
