package br.com.finalelite.myocardium.common.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class MathUtilities {

    public static double trim(int degree, double value) {
        StringBuilder format;
        if (degree == 0) {
            format = new StringBuilder("#");
        } else {
            format = new StringBuilder("#.#");
        }

        for (int i = 1; i < degree; i++) {
            format.append("#");
        }

        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.CANADA);
        DecimalFormat decimalFormat = new DecimalFormat(format.toString(), formatSymbols);

        return Double.valueOf(decimalFormat.format(value));
    }

}
