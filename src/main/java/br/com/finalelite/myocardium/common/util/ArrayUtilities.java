package br.com.finalelite.myocardium.common.util;

import br.com.finalelite.myocardium.common.Group;

import java.util.Collections;
import java.util.HashSet;

public class ArrayUtilities {

    public static Group[] sumRankArrays(Group[]... arrays) {
        HashSet<Group> groupHashSet = new HashSet<>();
        for (Group[] array : arrays) {
            Collections.addAll(groupHashSet, array);
        }
        Group[] sumArray = new Group[groupHashSet.size()];
        return groupHashSet.toArray(sumArray);

    }

}
