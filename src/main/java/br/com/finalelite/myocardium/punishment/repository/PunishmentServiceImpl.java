package br.com.finalelite.myocardium.punishment.repository;

import br.com.finalelite.myocardium.punishment.PunishmentManager;
import br.com.finalelite.myocardium.punishment.repository.token.GetPunishmentsToken;
import br.com.finalelite.myocardium.punishment.repository.token.PunishPlayerToken;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import br.com.finalelite.myocardium.punishment.repository.token.RevokeToken;
import br.com.finalelite.myocardium.web.rsi.RestfulImpl;
import br.com.finalelite.myocardium.web.rsi.thread.CallThread;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PunishmentServiceImpl extends RestfulImpl {

    @Getter
    private PunishmentManager punishmentManager;

    @Getter
    private String url;

    public PunishmentServiceImpl(PunishmentManager punishmentManager, String url) {
        super(new CallThread());

        this.punishmentManager = punishmentManager;
        this.url = url;
    }

    public PunishmentToken punishPlayer(PunishPlayerToken token) throws ExecutionException, InterruptedException {
        return postForObject(url, token, PunishmentToken.class).get().getData();
    }

    public List<PunishmentToken> getPunishments(GetPunishmentsToken token) throws ExecutionException, InterruptedException {
        PunishmentToken[] punishmentArray;
        punishmentArray = getForObject(url + "/" + token.player, PunishmentToken[].class).get().getData();
        return Arrays.asList(punishmentArray);
    }

    public PunishmentToken revokePunishment(RevokeToken token) throws ExecutionException, InterruptedException {
        return putForObject(url + "/" + token.id, token, PunishmentToken.class).get().getData();
    }

}
