package br.com.finalelite.myocardium.punishment.internal.processors;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.account.Account;
import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import br.com.finalelite.myocardium.internal.mqtt.message.Processor;
import br.com.finalelite.myocardium.punishment.Punishment;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MuteProcessor implements Processor {


    @Override
    public void process(Message message) {
        System.out.println("reached mute");
        Player player = Bukkit.getPlayer(message.getPlayerName());
        if (player != null) {

            Punishment punishment = loadPunishment(message);

            Account account = Core.accountManager.getAccount(player);
            account.setMuted(true, punishment.getDuration() > 0 ? punishment.getTime() + punishment.getDuration() : -1);

            player.sendMessage("");
            player.sendMessage(ChatColor.RED + " (!) Um silenciamento foi aplicado à sua conta.");
            player.sendMessage(ChatColor.YELLOW + " (!) Digite /conta para mais informações.");
            player.sendMessage("");

            Bukkit.broadcastMessage(ChatColor.RED + " (!) Um jogador neste servidor foi silenciado.");
            Bukkit.broadcastMessage(ChatColor.RED + " (!) Siga as Regras da Comunidade para não ser o próximo.");

        }
    }

    private Punishment loadPunishment(Message message) {
        PunishmentToken punishmentToken = Core.getGson().fromJson(message.getValue(), PunishmentToken.class);
        return new Punishment(punishmentToken);
    }
}
