package br.com.finalelite.myocardium.punishment.commands;

import br.com.finalelite.myocardium.account.repository.token.AccountToken;
import br.com.finalelite.myocardium.commands.CommandBase;
import br.com.finalelite.myocardium.common.Group;
import br.com.finalelite.myocardium.common.util.TimeUtilities;
import br.com.finalelite.myocardium.menu.MenuInventory;
import br.com.finalelite.myocardium.menu.MenuItem;
import br.com.finalelite.myocardium.punishment.Punishment;
import br.com.finalelite.myocardium.punishment.PunishmentManager;
import br.com.finalelite.myocardium.punishment.enumerators.PunishmentReason;
import br.com.finalelite.myocardium.punishment.repository.token.GetPunishmentsToken;
import br.com.finalelite.myocardium.punishment.repository.token.PunishPlayerToken;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class PunishCommand extends CommandBase<PunishmentManager> {


    public PunishCommand(PunishmentManager module) {
        super(module, Group.STAFF, "punir", "punish");
    }

    @SuppressWarnings({"Duplicates", "deprecation"})
    @Override
    public void execute(Player player, String[] args) {
        if (args == null || args.length < 1) {
            player.sendMessage(ChatColor.RED + "Uso: /" + aliasUsed + " [jogador]");
            return;
        }

        player.sendMessage(ChatColor.GRAY + "Enviando requisição...");

        AccountToken accountToken;
        List<Punishment> punishmentList;

        try {
            GetPunishmentsToken getPunishmentsToken = new GetPunishmentsToken();
            getPunishmentsToken.player = args[0];

            accountToken = this.module.getAccountManager().getAccountService().getAccount(getPunishmentsToken.player);
            punishmentList = this.module.loadTokens(this.module.getPunishmentService().getPunishments(getPunishmentsToken));
        } catch (InterruptedException | ExecutionException e) {
            player.sendMessage(ChatColor.RED + "Ocorreu um erro ao recuperar informações do servidor web.");
            e.printStackTrace();
            return;
        }

        if (accountToken == null) {
            player.sendMessage(ChatColor.RED + "O jogador nunca entrou no servidor.");
            return;
        }

        MenuInventory menuInventory = new MenuInventory("Punindo: " + accountToken.nickname, 6, true);
        int currentSlot = 9;

        for (PunishmentReason reason : PunishmentReason.values()) {

            Material material = reason.getItem();
            ItemStack item = new ItemStack(material);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + reason.name().replaceAll("_", " "));
            String[] lore = new String[]{
                    "",
                    ChatColor.WHITE + "Motivo: " + ChatColor.YELLOW + reason.getReason(),
                    ChatColor.WHITE + "Duração: " + ChatColor.YELLOW + TimeUtilities.makeString(reason.getDuration()),
                    ChatColor.WHITE + "Tipo: " + ChatColor.YELLOW + reason.getType().name().substring(0, 1) +
                            reason.getType().name().substring(1).toLowerCase(),
                    "",
                    ChatColor.GRAY + "Clique para aplicar a punição."
            };
            meta.setLore(Arrays.asList(lore));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);

            MenuItem menuItem = new MenuItem(item, (caller, inventory, clickType, itemStack, slot) -> {

                boolean success = false;

                PunishPlayerToken punishPlayerToken = new PunishPlayerToken(args[0], reason.getReason(), caller.getName(), reason.getType().name().toUpperCase(), reason.getDuration());
                caller.closeInventory();
                caller.sendMessage(ChatColor.GRAY + "Enviando requisição...");
                PunishmentToken token;

                try {

                    token = module.getPunishmentService().punishPlayer(punishPlayerToken);
                    if (token != null) {
                        success = true;
                    }

                } catch (ExecutionException | InterruptedException e) {
                    player.sendMessage(ChatColor.RED + "Ocorreu um erro ao registrar a punição no servidor web.");
                    e.printStackTrace();
                    return;
                }

                if (success) {
                    player.sendMessage(ChatColor.GREEN + "Jogador punido com sucesso.");
                    module.getPunishmentMessenger().broadcastPunishment(token);
                }


            });
            menuInventory.setItem(menuItem, currentSlot);
            currentSlot++;
        }


        int currentPunishmentSlot = 36;
        for (Punishment punishment : punishmentList) {

            Material material;

            if (punishment.isActive()) {
                material = Material.REDSTONE_BLOCK;
            } else if (punishment.isRevoked()) {
                material = Material.GLASS;
            } else {
                material = Material.COAL_BLOCK;
            }

            ItemStack item = new ItemStack(material);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.WHITE + "Punição " + ChatColor.AQUA + "#" + punishment.getId());

            String[] lore = new String[]{
                    "",
                    ChatColor.WHITE + "Motivo: " + ChatColor.YELLOW + punishment.getReason(),
                    ChatColor.WHITE + "Autor: " + ChatColor.YELLOW + punishment.getAuthor(),
                    ChatColor.WHITE + "Tipo: " + ChatColor.YELLOW + punishment.getType().name().substring(0, 1)
                            + punishment.getType().name().substring(1).toLowerCase(),
                    "",
                    ChatColor.WHITE + "Duração: " + ChatColor.YELLOW + TimeUtilities.makeString(punishment.getDuration()),
                    ChatColor.WHITE + "Data: " + ChatColor.YELLOW + TimeUtilities.when(punishment.getTime()),
                    "",
                    ChatColor.WHITE + "Status: " + ChatColor.YELLOW + (punishment.isActive() ? "Ativo" : "Expirado")
            };

            List<String> tempList = Arrays.asList(lore);
            ArrayList<String> loreProccessed = new ArrayList<>(tempList);
            if (punishment.isRevoked()) {
                loreProccessed.add("");
                loreProccessed.add(ChatColor.WHITE + "Punição Revogada");
                loreProccessed.add(ChatColor.WHITE + "Autor: " + ChatColor.YELLOW + punishment.getRevokeAuthor());
                loreProccessed.add(ChatColor.WHITE + "Motivo: " + ChatColor.YELLOW + punishment.getRevokeReason());
                loreProccessed.add(ChatColor.WHITE + "Data: " + ChatColor.YELLOW + TimeUtilities.when(punishment.getRevokeTime()));
                loreProccessed.add("");
            }

            meta.setLore(loreProccessed);
            item.setItemMeta(meta);

            menuInventory.setItem(item, currentPunishmentSlot);
            currentPunishmentSlot++;

        }

        ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
        skull = Bukkit.getUnsafe().modifyItemStack(skull, "{SkullOwner:\"" + accountToken.nickname + "\"}");
        ItemMeta skullMeta = skull.getItemMeta();
        skullMeta.setDisplayName(ChatColor.BLUE + "Punindo o jogador " + accountToken.nickname);

        String[] lore = new String[]{
                "",
                ChatColor.GRAY + "ID: " + ChatColor.RESET + accountToken.id,
                ChatColor.GRAY + "Nickname: " + ChatColor.RESET + accountToken.nickname,
                ChatColor.GRAY + "Grupos: " + ChatColor.RESET + accountToken.groups,
                ChatColor.GRAY + "Criado em: " + ChatColor.RESET + accountToken.created_at,
                ChatColor.GRAY + "Atualizado em: " + ChatColor.RESET + accountToken.updated_at,
                "",
                ChatColor.GRAY + "Punições totais: " + ChatColor.RESET + punishmentList.size()
        };

        skullMeta.setLore(Arrays.asList(lore));
        skull.setItemMeta(skullMeta);

        menuInventory.setItem(skull, 4);
        menuInventory.createAndOpenInventory(player);


    }

    @Override
    public List<String> onTabComplete(Player player, String command, String[] args) {
        return null;
    }


}
