package br.com.finalelite.myocardium.punishment.commands;

import br.com.finalelite.myocardium.account.repository.token.AccountToken;
import br.com.finalelite.myocardium.commands.CommandBase;
import br.com.finalelite.myocardium.common.Group;
import br.com.finalelite.myocardium.common.util.ArrayUtilities;
import br.com.finalelite.myocardium.common.util.TimeUtilities;
import br.com.finalelite.myocardium.menu.MenuInventory;
import br.com.finalelite.myocardium.menu.MenuItem;
import br.com.finalelite.myocardium.punishment.Punishment;
import br.com.finalelite.myocardium.punishment.PunishmentManager;
import br.com.finalelite.myocardium.punishment.repository.token.GetPunishmentsToken;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import br.com.finalelite.myocardium.punishment.repository.token.RevokeToken;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class RevokeCommand extends CommandBase<PunishmentManager> {


    public RevokeCommand(PunishmentManager module) {
        super(module, ArrayUtilities.sumRankArrays(Group.LEADERSHIP, new Group[]{Group.ADMINISTRADOR}), "revoke", "revogar", "despunir", "unpunish");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(Player player, String[] args) {
        if (args == null || args.length < 2) {
            player.sendMessage(ChatColor.RED + "Uso: /" + aliasUsed + " [jogador] [motivo]");
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            if (stringBuilder.length() == 0) {
                stringBuilder.append(args[i]);
            } else {
                stringBuilder.append(" ").append(args[i]);
            }
        }
        String reason = stringBuilder.toString();


        player.sendMessage(ChatColor.GRAY + "Enviando requisição...");

        AccountToken accountToken;
        List<Punishment> punishmentList;

        try {
            GetPunishmentsToken getPunishmentsToken = new GetPunishmentsToken();
            getPunishmentsToken.player = args[0];

            accountToken = this.module.getAccountManager().getAccountService().getAccount(getPunishmentsToken.player);
            punishmentList = this.module.loadTokens(this.module.getPunishmentService().getPunishments(getPunishmentsToken));
        } catch (InterruptedException | ExecutionException e) {
            player.sendMessage(ChatColor.RED + "Ocorreu um erro ao recuperar informações do servidor web.");
            e.printStackTrace();
            return;
        }

        if (accountToken == null) {
            player.sendMessage(ChatColor.RED + "O jogador nunca entrou no servidor.");
            return;
        }


        if (punishmentList.isEmpty() || punishmentList.stream().noneMatch(Punishment::isActive)) {
            player.sendMessage(ChatColor.RED + "O jogador não possui punições para serem revogadas.");
            return;
        }

        MenuInventory menuInventory = new MenuInventory("Revogando punições: " + accountToken.nickname, 6, true);
        int currentSlot = 18;

        for (Punishment punishment : punishmentList) {

            if (!punishment.isActive()) continue;

            ItemStack itemStack = new ItemStack(Material.REDSTONE);
            ItemMeta itemMeta = itemStack.getItemMeta();

            itemMeta.setDisplayName(ChatColor.WHITE + "Punição " + ChatColor.AQUA + "#" + punishment.getId());

            String[] lore = new String[]{
                    "",
                    ChatColor.WHITE + "Motivo: " + ChatColor.YELLOW + punishment.getReason(),
                    ChatColor.WHITE + "Duração: " + ChatColor.YELLOW + TimeUtilities.makeString(punishment.getDuration()),
                    ChatColor.WHITE + "Tipo: " + ChatColor.YELLOW + punishment.getType().name().substring(0, 1) +
                            punishment.getType().name().substring(1).toLowerCase(),
                    "",
                    ChatColor.GRAY + "Clique para revogar a punição."
            };

            itemMeta.setLore(Arrays.asList(lore));
            itemStack.setItemMeta(itemMeta);

            MenuItem menuItem = new MenuItem(itemStack, (caller, inventory, clickType, item, slot) -> {

                boolean success = false;

                caller.closeInventory();
                caller.sendMessage(ChatColor.GRAY + "Enviando requisição...");

                RevokeToken revokeToken = new RevokeToken();
                revokeToken.id = punishment.getId();
                revokeToken.revoke_author = caller.getName();
                revokeToken.revoke_reason = reason;

                PunishmentToken token;

                try {
                    token = this.module.getPunishmentService().revokePunishment(revokeToken);


                    if (token != null) {
                        success = true;
                    }

                } catch (InterruptedException | ExecutionException e) {
                    player.sendMessage(ChatColor.RED + "Ocorreu um erro ao registrar a remoção no servidor web.");
                    e.printStackTrace();
                    return;
                }

                if (success) {
                    player.sendMessage(ChatColor.GREEN + "Punição revogada com sucesso.");
                    this.module.getPunishmentMessenger().broadcastRevoke(revokeToken, accountToken.nickname);
                }

            });

            menuInventory.setItem(menuItem, currentSlot);
            currentSlot++;

        }

        ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
        skull = Bukkit.getUnsafe().modifyItemStack(skull, "{SkullOwner:\"" + accountToken.nickname + "\"}");
        ItemMeta skullMeta = skull.getItemMeta();
        skullMeta.setDisplayName(ChatColor.BLUE + "Revogando punição do jogador " + accountToken.nickname);

        String[] lore = new String[]{
                "",
                ChatColor.GRAY + "ID: " + ChatColor.RESET + accountToken.id,
                ChatColor.GRAY + "Nickname: " + ChatColor.RESET + accountToken.nickname,
                ChatColor.GRAY + "Grupos: " + ChatColor.RESET + accountToken.groups,
                ChatColor.GRAY + "Criado em: " + ChatColor.RESET + accountToken.created_at,
                ChatColor.GRAY + "Atualizado em: " + ChatColor.RESET + accountToken.updated_at,
                "",
                ChatColor.GRAY + "Punições totais: " + ChatColor.RESET + punishmentList.size(),
                ChatColor.GRAY + "Motivo da Revogação: " + ChatColor.RESET + reason
        };

        skullMeta.setLore(Arrays.asList(lore));
        skull.setItemMeta(skullMeta);

        menuInventory.setItem(skull, 4);
        menuInventory.createAndOpenInventory(player);


    }

    @Override
    public List<String> onTabComplete(Player player, String command, String[] args) {
        return null;
    }
}
