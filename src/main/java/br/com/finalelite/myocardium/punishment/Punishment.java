package br.com.finalelite.myocardium.punishment;

import br.com.finalelite.myocardium.punishment.enumerators.PunishmentType;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import br.com.finalelite.myocardium.punishment.repository.token.RevokeToken;
import lombok.Data;

@Data
public class Punishment {

    private int id;
    private String player;
    private String reason;
    private String author;
    private PunishmentType type;

    private long time;
    private long duration;

    private boolean revoked;

    private String revokeReason;
    private String revokeAuthor;
    private long revokeTime;

    public Punishment(PunishmentToken token) {
        this.id = token.getId();
        this.player = token.getPlayer();
        this.reason = token.getReason();
        this.author = token.getAuthor();
        this.type = PunishmentType.valueOf(token.getType().toUpperCase());
        this.time = token.getTime();
        this.duration = token.getDuration();
        this.revoked = token.getRevoked() != 0;
        this.revokeReason = token.getRevoke_reason();
        this.revokeAuthor = token.getRevoke_author();
        this.revokeTime = token.getRevoke_time();
    }

    public boolean isActive() {
        if (revoked) return false;
        if (duration < 0) return true;
        return System.currentTimeMillis() < (duration + time);
    }

    public void revoke(RevokeToken token) {
        revoked = true;
        revokeReason = token.revoke_reason;
        revokeAuthor = token.revoke_author;
        revokeTime = System.currentTimeMillis();
    }


}
