package br.com.finalelite.myocardium.punishment.internal;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import br.com.finalelite.myocardium.internal.mqtt.message.MessageType;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import br.com.finalelite.myocardium.punishment.repository.token.RevokeToken;

public class PunishmentMessenger  {

    public void broadcastPunishment(PunishmentToken token) {
        MessageType messageType = token.getType().equalsIgnoreCase("BAN") ? MessageType.BANUSER : MessageType.MUTEUSER;
        Message message = new Message(messageType, token.getPlayer(), Core.getGson().toJson(token));
        Core.mqttPublisher.sendMessage(message.build(), true);
    }

    public void broadcastRevoke(RevokeToken token, String playerName) {
        MessageType messageType = MessageType.FORGIVE;
        Message message = new Message(messageType, playerName, Core.getGson().toJson(token));
        Core.mqttPublisher.sendMessage(message.build(), true);
    }

}
