package br.com.finalelite.myocardium.punishment;

import br.com.finalelite.myocardium.account.Account;
import br.com.finalelite.myocardium.account.AccountManager;
import br.com.finalelite.myocardium.common.util.TimeUtilities;
import br.com.finalelite.myocardium.common.util.builder.KickMessageBuilder;
import br.com.finalelite.myocardium.module.Module;
import br.com.finalelite.myocardium.punishment.commands.PunishCommand;
import br.com.finalelite.myocardium.punishment.commands.RevokeCommand;
import br.com.finalelite.myocardium.punishment.enumerators.PunishmentType;
import br.com.finalelite.myocardium.punishment.internal.PunishmentMessenger;
import br.com.finalelite.myocardium.punishment.repository.PunishmentServiceImpl;
import br.com.finalelite.myocardium.punishment.repository.token.GetPunishmentsToken;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class PunishmentManager extends Module {

    private PunishmentServiceImpl punishmentService;

    @Getter
    private PunishmentMessenger punishmentMessenger;

    @Getter
    private Map<String, Long> mutedPlayers;

    @Getter
    private AccountManager accountManager;

    public PunishmentManager(AccountManager accountManager) {
        super("Punições", accountManager.getPlugin());

        this.accountManager = accountManager;
    }

    @Override
    public void onEnable() {
        this.punishmentService = new PunishmentServiceImpl(this, System.getProperty("core.webserver") + "/punishment");
        this.mutedPlayers = new HashMap<>();
        this.punishmentMessenger = new PunishmentMessenger();
    }

    @Override
    public void addCommands() {
        addCommand(new PunishCommand(this));
        addCommand(new RevokeCommand(this));
    }

    public PunishmentServiceImpl getPunishmentService() {
        return punishmentService;
    }

    public List<Punishment> loadTokens(List<PunishmentToken> punishmentTokenList) {
        List<Punishment> punishments = new ArrayList<>();
        punishmentTokenList.forEach(token -> punishments.add(new Punishment(token)));
        return punishments;
    }

    public void computeMute(Account account) {
        if (mutedPlayers.containsKey(account.getNickname().toLowerCase())) {
            account.setMuted(true, mutedPlayers.get(account.getNickname().toLowerCase()));
            mutedPlayers.remove(account.getNickname().toLowerCase());
        }
    }


    @SuppressWarnings("Duplicates")
    public void validateSession(AsyncPlayerPreLoginEvent event) {

        GetPunishmentsToken getToken = new GetPunishmentsToken();
        getToken.player = event.getName();

        List<PunishmentToken> punishmentTokens;

        try {
            punishmentTokens = punishmentService.getPunishments(getToken);
        } catch (ExecutionException | InterruptedException e) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "Ocorreu um erro durante a consulta de punições existentes.");
            return;
        }

        List<Punishment> punishments = new ArrayList<>();
        punishmentTokens.forEach(token -> punishments.add(new Punishment(token)));

        if (!punishments.isEmpty()) {
            for (Punishment punishment : punishments) {
                if (punishment.isActive() && punishment.getType() == PunishmentType.BAN) {

                    KickMessageBuilder kickMessageBuilder = new KickMessageBuilder();
                    kickMessageBuilder.addLine("&cSua conta foi suspensa do servidor.").addLine()
                            .addLine("&fMotivo: &e" + punishment.getReason())
                            .addLine("&fDuração: &e" + TimeUtilities.makeString(punishment.getDuration()));

                    if (punishment.getDuration() > 0) {
                        kickMessageBuilder.addLine("&8&o(" + TimeUtilities.makeString(
                                (punishment.getDuration() + punishment.getTime()) - System.currentTimeMillis()
                        ) + " restantes)");

                    }

                    kickMessageBuilder.addLine().addLine("&fUse o ID &b#" + punishment.getId() + " &fcaso precise de revisão.");


                    event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, kickMessageBuilder.build());
                    return;
                }

                if (punishment.isActive() && punishment.getType() == PunishmentType.MUTE) {
                    mutedPlayers.put(event.getName().toLowerCase(), punishment.getDuration() > 0 ? punishment.getTime() + punishment.getDuration() : -1);
                }

            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void processChat(AsyncPlayerChatEvent event) {
        if (accountManager.getAccount(event.getPlayer()).isMuted()
                && (System.currentTimeMillis() < accountManager.getAccount(event.getPlayer()).getExpireTime() || (accountManager.getAccount(event.getPlayer()).getExpireTime() < 0))) {

            Player player = event.getPlayer();
            player.sendMessage("");
            player.sendMessage(ChatColor.RED + "(!) Você está silenciado.");
            player.sendMessage(ChatColor.YELLOW + "(!) Digite /conta para detalhes da punição.");
            player.sendMessage("");

            event.setCancelled(true);
        }
    }

}
