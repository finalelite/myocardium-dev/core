package br.com.finalelite.myocardium.punishment.enumerators;

import lombok.Getter;
import org.bukkit.Material;

public enum  PunishmentReason {

    CHEATING(PunishmentType.BAN, "Uso de Trapaças", Material.IRON_SWORD, 50000),
    TEST(PunishmentType.BAN, "Teste", Material.BARRIER, 50000),
    MUTE(PunishmentType.MUTE, "Mute", Material.BOOK, 30000);

    @Getter private PunishmentType type;
    @Getter private String reason;
    @Getter private Material item;
    @Getter private long duration;


    PunishmentReason(PunishmentType type, String reason, Material item, long duration) {
        this.type = type;
        this.reason = reason;
        this.item = item;
        this.duration = duration;
    }
}
