package br.com.finalelite.myocardium.punishment.internal.processors;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.account.Account;
import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import br.com.finalelite.myocardium.internal.mqtt.message.Processor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RevokeProcessor implements Processor {


    @Override
    public void process(Message message) {
        Player player = Bukkit.getPlayer(message.getPlayerName());
        if (player != null) {

            Account account = Core.accountManager.getAccount(player);
            account.setMuted(false, 1);

            player.sendMessage("");
            player.sendMessage(ChatColor.GREEN + "(!) Você não está mais silenciado.");
            player.sendMessage(ChatColor.YELLOW + "(!) Siga as regras da comunidade para evitar punições.");
            player.sendMessage("");

        }
    }
}
