package br.com.finalelite.myocardium.punishment.internal.processors;

import br.com.finalelite.myocardium.Core;
import br.com.finalelite.myocardium.common.util.TimeUtilities;
import br.com.finalelite.myocardium.common.util.builder.KickMessageBuilder;
import br.com.finalelite.myocardium.internal.mqtt.message.Message;
import br.com.finalelite.myocardium.internal.mqtt.message.Processor;
import br.com.finalelite.myocardium.punishment.Punishment;
import br.com.finalelite.myocardium.punishment.repository.token.PunishmentToken;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BanProcessor implements Processor {


    @Override
    public void process(Message message) {
        Player player = Bukkit.getPlayer(message.getPlayerName());
        if (player != null) {

            Punishment punishment = loadPunishment(message);

            KickMessageBuilder kickMessageBuilder = new KickMessageBuilder();
            kickMessageBuilder.addLine("&cSua conta foi suspensa do servidor.").addLine()
                    .addLine("&fMotivo: &e" + punishment.getReason())
                    .addLine("&fDuração: &e" + TimeUtilities.makeString(punishment.getDuration()));

            if (punishment.getDuration() > 0) {
                kickMessageBuilder.addLine("&8&o(" + TimeUtilities.makeString(
                        (punishment.getDuration() + punishment.getTime()) - System.currentTimeMillis()
                ) + " restantes)");

            }

            kickMessageBuilder.addLine().addLine("&fUse o ID &b#" + punishment.getId() + " &fcaso precise de revisão.");

            new BukkitRunnable() {

                @Override
                public void run() {
                    player.kickPlayer(kickMessageBuilder.build());
                }
            }.runTask(Core.getInstance());

            Bukkit.broadcastMessage(ChatColor.RED + " (!) Um jogador neste servidor teve sua conta suspensa.");
            Bukkit.broadcastMessage(ChatColor.RED + " (!) Siga as Regras da Comunidade para não ser o próximo.");

        }
    }

    private Punishment loadPunishment(Message message) {
        PunishmentToken punishmentToken = Core.getGson().fromJson(message.getValue(), PunishmentToken.class);
        return new Punishment(punishmentToken);
    }
}
