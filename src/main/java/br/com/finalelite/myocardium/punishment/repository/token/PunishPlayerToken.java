package br.com.finalelite.myocardium.punishment.repository.token;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PunishPlayerToken {

    public String player;
    public String reason;
    public String author;
    public String type;
    public long duration = -1;

}
