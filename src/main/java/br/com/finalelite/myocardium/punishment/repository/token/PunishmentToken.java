package br.com.finalelite.myocardium.punishment.repository.token;

import lombok.Data;

@Data
public class PunishmentToken {

    private int id;
    private String player;
    private String reason;
    private String author;
    private String type;
    private long duration;
    private int revoked;
    private String revoke_reason;
    private String revoke_author;
    private long time;
    private long revoke_time;
    private String created_at;
    private String updated_at;

}
